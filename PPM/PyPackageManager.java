import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class PyPackageManager{
	public static String packages[] = {
		"beautifulsoup4==4.4.1",
		"boto==2.48.0",
		"bz2file==0.98",
		"certifi==2017.7.27.1",
		"chardet==3.0.4",
		"gensim==2.3.0",
		"html5lib==0.999",
		"idna==2.5",
		"nltk==3.2.4",
		"numpy==1.13.1",
		"pexpect==4.0.1",
		"pip==9.0.1",
		"ptyprocess==0.5",
		"pyxdg==0.25",
		"reportlab==3.3.0",
		"requests==2.18.3",
		"scipy==0.19.1",
		"setuptools==20.7.0",
		"six==1.10.0",
		"smart-open==1.5.3",
		"textblob==0.12.0",
		"twitter==1.17.1",
		"urllib3==1.22"
	};
    public static void main(String args[]) throws IOException{
		//Reading package.json file 
		String packageJson = new String(Files.readAllBytes(Paths.get("package.json")));
		String refinedJSON = refineJson(packageJson);									
		ArrayList<String> commands = getInstallCommands(refinedJSON);		
		//ArrayList<String> commands = getUnInstallCommands(packageJson);
		installPackages(commands);
    }

    public static String refineJson(String packageJson){
    	String whiteSameOminted = packageJson.replaceAll("\\s","");
    	String shitOmmited = "";
    	for(int i=0;i<whiteSameOminted.length();i++){
    		char c = whiteSameOminted.charAt(i);
    		int charValue = (int)c;
    		if(charValue>128){
    			continue;
    		}
    		shitOmmited += c;
    	}
    	return shitOmmited;
    }

    public static ArrayList<String> getInstallCommands() throws IOException{
    	ArrayList<String> commands = new ArrayList<String>();
    	//commands.add({"export CLASSPATH=\"$(pwd)\""});
    	//commands.add("export CLASSPATH=\"$(pwd)\"");
    	for(int i=0;i<packages.length;i++){
			commands.add("pip install "+packages[i]);
		}
		return commands;
    }

    public static ArrayList<String> getUnInstallCommands(){
		ArrayList<String> commands = new ArrayList<String>();
		for(int i=0;i<packages.length;i++){
			commands.add("pip uninstall "+packages[i]);
		}
		return commands;
    }

    public static ArrayList<String> getInstallCommands(String inputJson) throws JSONException{
    	ArrayList<String> commandsList = new ArrayList<String>();
    	try{
	    	JSONObject jsonObject = new JSONObject(inputJson);
	    	JSONArray packages = jsonObject.getJSONArray("Dependencies");
	    	for(int i=0;i<packages.length();i++){
	    		commandsList.add("pip install "+packages.getString(i));
	    	}
    	}
    	catch(JSONException e){
    		System.out.println(e.toString());
    	}
		return commandsList;
    }

    public static String trimBraces(String str){
    	return str.substring(1,str.length()-1);
    }

    public static void installPackages(ArrayList<String> commands) throws IOException{
    	ArrayList<String> failedPackages = new ArrayList<String>();
    	while(commands.isEmpty() == false){
    		Process p = Runtime.getRuntime().exec(commands.get(0)/*+" --verbose"*/);
			boolean hadSuccessfullyInstalled = runCommand(commands.get(0),p);
			if(hadSuccessfullyInstalled){
				commands.remove(0);
			}
			else{
				System.out.println("####################Failure:"+"Some problem in installation####################");
				failedPackages.add(commands.remove(0));
			}
    	}
    	if(failedPackages.isEmpty()){
    		System.out.println("********************Sucess:"+"All packages are successfully installed********************");
	    }
	    else{
	    	System.out.println("********************packages failed to install********************");
	    	while(failedPackages.isEmpty() == false){
	    		System.out.println(failedPackages.remove(0).split(" ")[2]);
	    	}
	    }
    }

    public static boolean runCommand(String command,Process p){
    	String s;		
		try {		    
		    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    while ((s = br.readLine()) != null)
				System.out.println(s);
		    p.waitFor();
		    String exitStaus = "";
		    if(p.exitValue() == 0){
		    	exitStaus = "Sucess";
		    	p.destroy();
		    	return true;
		    }
		    else{
		    	exitStaus = "Fail";	
		    	p.destroy();
		    	return false;
		    }
		} catch (Exception e) {
			System.out.println(e.toString());		
			return false;
		}
    }
}
